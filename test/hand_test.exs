defmodule HandTest do
  use ExUnit.Case

  describe "Hand analysis:" do
    test "Correctly analyses `two pairs` hand" do
      assert Hand.analyze([
               {:jack, :hearts},
               {3, :spades},
               {:jack, :clubs},
               {3, :clubs},
               {2, :hearts}
             ]) == %{
               is_same_suit: false,
               pairs: [[{3, :spades}, {3, :clubs}], [jack: :hearts, jack: :clubs]],
               sequences: [
                 [jack: :hearts],
                 [jack: :clubs],
                 [{3, :spades}],
                 [{3, :clubs}, {2, :hearts}]
               ],
               the_hand: [
                 {:jack, :hearts},
                 {3, :spades},
                 {:jack, :clubs},
                 {3, :clubs},
                 {2, :hearts}
               ]
             }
    end

    # more tests to-do
  end

  describe "Hand ranking:" do
    test "Detects `Straight flush` correctly" do
      assert Hand.rank([
               {9, :spades},
               {10, :spades},
               {8, :spades},
               {:jack, :spades},
               {7, :spades}
             ]) == 1
    end

    test "Detects `Four of a kind` correctly" do
      assert Hand.rank([
               {5, :spades},
               {2, :diamonds},
               {5, :diamonds},
               {5, :hearts},
               {5, :clubs}
             ]) == 2
    end

    test "Detects `Full house` correctly" do
      assert Hand.rank([
               {:king, :diamonds},
               {6, :hearts},
               {:king, :spades},
               {6, :clubs},
               {6, :spades}
             ]) == 3
    end

    test "Detects `Flush` correctly" do
      assert Hand.rank([
               {:jack, :diamonds},
               {3, :diamonds},
               {9, :diamonds},
               {8, :diamonds},
               {4, :diamonds}
             ]) == 4
    end

    test "Detects `Straight` correctly" do
      assert Hand.rank([
               {6, :spades},
               {10, :diamonds},
               {8, :hearts},
               {9, :spades},
               {7, :diamonds}
             ]) == 5
    end

    test "Detects `Three of a kind` correctly" do
      assert Hand.rank([
               {:queen, :spades},
               {2, :clubs},
               {9, :hearts},
               {:queen, :clubs},
               {:queen, :hearts}
             ]) == 6
    end

    test "Detects `Two pair` correctly" do
      assert Hand.rank([
               {:jack, :hearts},
               {3, :spades},
               {:jack, :clubs},
               {3, :clubs},
               {2, :hearts}
             ]) == 7
    end

    test "Detects `One pair` correctly" do
      assert Hand.rank([
               {7, :hearts},
               {10, :spades},
               {4, :spades},
               {8, :clubs},
               {10, :hearts}
             ]) == 8
    end

    test "Detects `High card` correctly" do
      assert Hand.rank([
               {:queen, :diamonds},
               {3, :hearts},
               {:king, :diamonds},
               {4, :clubs},
               {7, :clubs}
             ]) == 9
    end
  end

  describe "Hand list sorting:" do
    test "Compares correctly between two hands" do
      assert Hand.sort_hands([
               # high card
               [{:queen, :diamonds}, {3, :hearts}, {:king, :diamonds}, {4, :clubs}, {7, :clubs}],
               # three of a kind
               [{:queen, :spades}, {2, :clubs}, {9, :hearts}, {:queen, :clubs}, {:queen, :hearts}]
             ]) == [
               # three of a kind
               [
                 {:queen, :spades},
                 {2, :clubs},
                 {9, :hearts},
                 {:queen, :clubs},
                 {:queen, :hearts}
               ],
               # high card
               [{:queen, :diamonds}, {3, :hearts}, {:king, :diamonds}, {4, :clubs}, {7, :clubs}]
             ]
    end

    test "Sorts three flush hands correctly" do
      assert Hand.sort_hands([
               [{:jack, :hearts}, {3, :hearts}, {9, :hearts}, {8, :hearts}, {4, :hearts}],
               [
                 {4, :clubs},
                 {8, :clubs},
                 {:queen, :clubs},
                 {:jack, :clubs},
                 {10, :clubs}
               ],
               [{9, :spades}, {10, :spades}, {8, :spades}, {:ace, :spades}, {7, :spades}]
             ]) == [
               [{9, :spades}, {10, :spades}, {8, :spades}, {:ace, :spades}, {7, :spades}],
               [
                 {4, :clubs},
                 {8, :clubs},
                 {:queen, :clubs},
                 {:jack, :clubs},
                 {10, :clubs}
               ],
               [{:jack, :hearts}, {3, :hearts}, {9, :hearts}, {8, :hearts}, {4, :hearts}]
             ]
    end

    ## more sorting tests to-do
  end

  describe "Hand scoring:" do
    test "for a stright flush hand" do
      assert Hand.score([
               {9, :spades},
               {10, :spades},
               {8, :spades},
               {:jack, :spades},
               {7, :spades}
             ]) == 340
    end

    test "for a four of a kind hand" do
      assert Hand.score([
               {5, :spades},
               {2, :diamonds},
               {5, :diamonds},
               {5, :hearts},
               {5, :clubs}
             ]) == 1700
    end

    test "for a full house hand" do
      assert Hand.score([
               {:king, :diamonds},
               {6, :hearts},
               {:king, :spades},
               {6, :clubs},
               {6, :spades}
             ]) == 2588
    end

    test "for a flush hand" do
      assert Hand.score([
               {:jack, :diamonds},
               {3, :diamonds},
               {9, :diamonds},
               {8, :diamonds},
               {4, :diamonds}
             ]) == 3_301.937
    end

    test "for a straight hand" do
      assert Hand.score([
               {6, :spades},
               {10, :diamonds},
               {8, :hearts},
               {9, :spades},
               {7, :diamonds}
             ]) == 4400
    end

    test "for a three of a kind hand" do
      assert Hand.score([
               {:queen, :spades},
               {2, :clubs},
               {9, :hearts},
               {:queen, :clubs},
               {:queen, :hearts}
             ]) == 5280
    end

    test "for a two pair hand" do
      assert Hand.score([
               {:jack, :hearts},
               {3, :spades},
               {:jack, :clubs},
               {3, :clubs},
               {2, :hearts}
             ]) == 6328
    end

    test "for a one pair hand" do
      assert Hand.score([
               {7, :hearts},
               {10, :spades},
               {4, :spades},
               {8, :clubs},
               {10, :hearts}
             ]) == 7400
    end

    test "for a high card hand" do
      assert Hand.score([
               {:queen, :diamonds},
               {3, :hearts},
               {:king, :diamonds},
               {4, :clubs},
               {7, :clubs}
             ]) == 8170.187
    end
  end
end
