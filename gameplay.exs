#
# Vanilla version
#

{:ok, game} = Poker.game()

{:ok, game} = Poker.players([{"Nick", 1000}, {"Maria", 4000}], game)

{:ok, game} = Poker.deal(game)

#round 1
{:ok, game} = Poker.exchange(game, :spot_one, [{2, :clubs}])

#round 2
{:ok, game} = Poker.exchange(game, :spot_two, [{5, :diamonds}, {8, :spades}])

# settle_type = :winner | :tie
{:winner, winning_spot} = Poker.winner(game)

# if settle_type == :winner do
{:ok, winner = %Player{}} = Poker.player(winning_spot, game)

#
# Betting version
#

game = Poker.game()

{:ok, game} = Poker.players([{"Nick", 1000}, {"Maria", 4000}], game)

game = Poker.deal(players, buy_in)

# {stake: 10, [spot_one: 10, spot_two: 10]} = pot

#round 1
{:ok, game} = Poker.exchange(game, :spot_one, [{2, :clubs}])

Player.bet(game, :spot_one, 10)

#round 2
{:ok, game} = Poker.exchange(game, :spot_two, [{5, :diamonds}, {8, :spades}])

{:ok, game} = Player.raise(players, pot, :spot_two, 50)
{:ok, game} = Player.fold(players, :spot_one)

# winner
winning_spot = Poker.winner(players)

players = Player.award(players, winning_spot, Pot.winnings(pot)) # could be sum - ganiota(rake)


# maybe Player versions should be replaced with Poker versions,
# that use internally Player versions