defmodule Table do
  @moduledoc """
  The Table struct and functions to manipulate the spots.

  A struct of the players seats. There are two to six spots in this version of the poker game
  for players to "seat".

  Each table contains `spot_one`, `spot_two`, `spot_three`, `spot_four`, `spot_five`, `spot_six`.

  At each spot only a `%Player{}` struct can be placed.

  It cannot have less than two spots filled.
  """
  defstruct spot_one: nil,
            spot_two: nil,
            spot_three: nil,
            spot_four: nil,
            spot_five: nil,
            spot_six: nil

  @doc """
  Initializes players to consecutive spots as given in list.

  Because the table cannot contain less than two players the list cannot contain
  less than two players.
  """
  def init_players(table, player_list)

  def init_players(_table, player_list) when length(player_list) < 2 do
    {:error, "Table is constrained to have at least two players"}
  end

  def init_players(_table, player_list) when length(player_list) > 6 do
    {:error, "Table is constrained to have no more than six players"}
  end

  def init_players(table, player_list) do
    opts =
      Table.spots()
      |> Enum.zip(player_list)

    {:ok, struct(table, opts)}
  end

  @doc """
  Add a player at a certain spot

  The spot must be empty or it raises an exception.
  """
  def add_player!(table = %Table{}, spot, player = %Player{}) do
    if spot_empty?(table, spot) do
      Map.put(table, spot, player)
    else
      raise "THe spot is not empty"
    end
  end

  @doc """
  Checks if the Table has an empty free spot at the place specified
  """
  def spot_empty?(table, spot) do
    case Map.fetch(table, spot) do
      :error -> false
      {:ok, %Player{}} -> false
      {:ok, nil} -> true
    end
  end

  @doc """
  Returns the number of filled spots in the Table.

  The actual size is always 6 spots
  """
  def size(table = %Table{}) do
    table
    |> Map.from_struct()
    |> Enum.count(fn {_spot, player} -> player != nil end)
  end

  @doc """
  Returns the spot names of the Table struct in order

  ## Examples

      iex> Table.spots
      [:spot_one, :spot_two, :spot_three, :spot_four, :spot_five, :spot_six]

  """
  def spots do
    [:spot_one, :spot_two, :spot_three, :spot_four, :spot_five, :spot_six]
    # %Table{}
    # |> Map.from_struct
    # |> Map.keys
  end

  @doc """
  Returns the filled spot names only, in order

  ## Examples

      iex> Table.filled_spots(%Table{})
      []

      iex> Table.filled_spots(%Table{
      ...>       spot_one: %Player{name: "Vicky"},
      ...>       spot_two: nil,
      ...>       spot_three: nil,
      ...>       spot_four: %Player{name: "Paul", wallet: 100},
      ...>       spot_five: nil,
      ...>       spot_six: nil
      ...>     })
      [:spot_one, :spot_four]

  """
  def filled_spots(table = %Table{}) do
    table
    |> Map.from_struct()
    |> Enum.flat_map(fn {spot, pl} ->
      case pl do
        %Player{} -> [spot]
        nil -> []
      end
    end)
    |> Enum.reverse()
  end

  @doc """
  Applies a function on every spot of the Table (Player) and returns a list of tuples containing
  the spot name and the function result.

    ## Examples

      #iex> Table.spotify(%Table{}, )
      #[:spot_one, :spot_two, :spot_three, :spot_four, :spot_five, :spot_six]

  """
  def spotify(table = %Table{}, func) do
    table
    |> Map.from_struct()
    |> Enum.map(fn {spot, player} -> {spot, func.(player)} end)
  end

  @behaviour Access
  def fetch(table, key) do
    Map.fetch(table, key)
  end

  def get(table, key, default) do
    case fetch(table, key) do
      {:ok, value} -> value
      :error -> default
    end
  end

  def pop(table, key) do
    case fetch(table, key) do
      {:ok, value} -> {value, Map.put(table, key, nil)}
      :error -> {nil, table}
    end
  end

  def get_and_update(table, key, func) do
    Map.get_and_update(table, key, func)
  end
end
