defmodule Pokex.MixProject do
  use Mix.Project

  def project do
    [
      app: :pokex,
      version: "0.2.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      name: "Pokex",
      source_url: "https://bitbucket.org/vmanoli/pokex/",
      deps: deps(),
      test_coverage: [
        tool: Coverex.Task
      ],
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      env: [card_allotment: 5]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:coverex, "~> 1.4.10", only: :test},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false},
      {:credo, "~> 0.9.3", only: :dev, runtime: false},
      {:dialyxir, "~> 0.5.1", only: :dev}
      # {:mix_creator_helper, ">= 0.0.0", path: local_paths(:mix_creator_helper_path), only: :dev}
    ]
  end

  defp description() do
    "This is a poker engine implementing a library to play a *Five Hand Draw* poker variant."
  end

  defp package() do
    [
      licenses: ["Apache 2.0", "MIT"],
      links: %{"BitBucket" => "https://bitbucket.org/vmanoli/pokex"}
    ]
  end

  defp docs() do
    [
      main: "readme.html#content",
      logo: "./assets/images/cards_small.png",
      extras: ["README.md"]
    ]
  end
end
