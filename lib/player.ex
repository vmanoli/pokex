defmodule Player do
  @moduledoc """
  The Player model.

  Player is defined by `name`, `wallet` and his `hand` of cards.
  """
  @enforce_keys [:name]
  defstruct name: "", wallet: 0.0, hand: nil

  @doc """
  Returns a `%Player{}` struct from the tuple `{name, wallet}` given.

  `wallet` cannot be a negative number.

  Returns a tuple `{:ok, player}` or `{:error, "Reason"}`.

  ## Example

      iex> Player.create({"Maria", 1000.0})
      {:ok, %Player{name: "Maria", wallet: 1000.0, hand: nil}}

      iex> Player.create({"Maria", -1000.0})
      {:error, "Negative initial wallet amount not allowed"}

  """
  def create({_name, wallet}) when wallet < 0,
    do: {:error, "Negative initial wallet amount not allowed"}

  def create({name, wallet}) do
    {:ok, %Player{name: name, wallet: wallet}}
  end

  @doc """
  Adds a hand to the player.

  Overrides any previous hand in :hand
  """
  def hand(player = %Player{}, hand) do
    %Player{player | hand: hand}
  end

  @doc """
  Adds a hand to the player.

  Throws exception if there is a previous hand assigned
  """
  def hand!(player = %Player{hand: nil}, hand) do
    hand(player, hand)
  end

  def hand!(%Player{}, _hand) do
    raise "Hand already assigned"
  end

  @doc """
  Removes the cards in `cards` from player's hand and replaces them with cards in `for_cards`.

  Returns the `%Player{}` with the updated `:hand`.

  ## Examples

      iex> Player.exchange(%Player{name: "Nick", hand: [{6, :clubs}, {3, :spades}, {9, :spades}, {4, :diamonds}, {6, :hearts}]}, [{4, :diamonds}, {6, :hearts}], [{:ace, :spades}, {2, :hearts}])
      %Player{name: "Nick", hand: [{:ace, :spades}, {2, :hearts}, {6, :clubs}, {3, :spades}, {9, :spades}], wallet: 0.0}

  """
  def exchange(player = %Player{}, cards, for_cards) do
    new_hand = for_cards ++ (player.hand -- cards)
    %Player{player | hand: new_hand}
  end

  @doc """
  Adds the given amount to the player's wallet.

  The amount has to be a positive number.

  Returns the player.

  ## Examples

      iex> Player.deposit(%Player{name: "Nick"}, 50)
      %Player{name: "Nick", wallet: 50.0, hand: nil}

  """
  def deposit(player = %Player{}, amount) do
    do_deposit(player, amount)
  end

  defp do_deposit(_player, amount) when amount < 0 do
    raise ArgumentError, "Cannot deposit negative numbers"
  end

  defp do_deposit(player, amount) do
    %Player{player | wallet: player.wallet + amount}
  end

  @doc """
  Withdraws the amount given from the player's wallet.

  The amount has to be a positive number.

  The amount cannot be more than the player's balance (wallet).

  Returns the player.

  ## Examples

      iex> Player.withdraw(%Player{name: "Maria", wallet: 100.0}, 30)
      %Player{name: "Maria", wallet: 70.0, hand: nil}

      iex> Player.withdraw(%Player{name: "Nick", wallet: 100.0}, 100.0)
      %Player{name: "Nick", wallet: 0.0, hand: nil}

  """
  def withdraw(player = %Player{}, amount) do
    do_withdraw(player, amount)
  end

  defp do_withdraw(_player, amount) when amount < 0 do
    raise ArgumentError, "Cannot withdraw a negative amount"
  end

  defp do_withdraw(%Player{wallet: wallet} = player, amount) when wallet < amount do
    raise ArgumentError,
          "Cannot withdraw more than the amount in the players wallet #{inspect(player)}"
  end

  defp do_withdraw(player, amount) do
    %Player{player | wallet: player.wallet - amount}
  end
end
