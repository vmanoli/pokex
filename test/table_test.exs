defmodule TableTest do
  use ExUnit.Case

  doctest Table

  describe "Table spot empty" do
    test "returns true when spot is empty" do
      t = %Table{spot_one: %Player{name: "Vicky"}}
      assert Table.spot_empty?(t, :spot_two) == true
    end

    test "returns false when spot is taken" do
      t = %Table{spot_one: %Player{name: "Vicky"}}
      assert Table.spot_empty?(t, :spot_one) == false
    end

    test "returns false when spot name was not found" do
      t = %Table{spot_one: %Player{name: "Vicky"}}
      assert Table.spot_empty?(t, :spot_ten) == false
    end
  end

  describe "Table add player" do
    test "adds a player at the specified spot" do
      t = %Table{spot_one: %Player{name: "Vicky"}}
      p = %Player{name: "Paul", wallet: 100}

      assert Table.add_player!(t, :spot_four, p) ==
               %Table{
                 spot_one: %Player{name: "Vicky"},
                 spot_two: nil,
                 spot_three: nil,
                 spot_four: %Player{name: "Paul", wallet: 100},
                 spot_five: nil,
                 spot_six: nil
               }
    end

    test "raises an exception when the spot is not empty" do
      t = %Table{spot_one: %Player{name: "Vicky"}}
      p = %Player{name: "Paul", wallet: 100}

      assert_raise RuntimeError, fn -> Table.add_player!(t, :spot_one, p) end
    end
  end

  describe "Table init players" do
    test "adds three players in the table structure" do
      assert Table.init_players(%Table{}, [
               %Player{name: "Vicky"},
               %Player{name: "Paul", wallet: 100},
               %Player{name: "Zen", wallet: 990}
             ]) ==
               {:ok,
                %Table{
                  spot_one: %Player{name: "Vicky"},
                  spot_two: %Player{name: "Paul", wallet: 100},
                  spot_three: %Player{name: "Zen", wallet: 990},
                  spot_four: nil,
                  spot_five: nil,
                  spot_six: nil
                }}
    end

    test "returns :error when players are less than 2" do
      assert Table.init_players(%Table{}, [%Player{name: "Zen", wallet: 990}]) ==
               {:error, "Table is constrained to have at least two players"}
    end

    test "returns :error when players are more than 6" do
      assert Table.init_players(%Table{}, [
               %Player{name: "Vicky"},
               %Player{name: "Paul", wallet: 100},
               %Player{name: "Zen", wallet: 990},
               %Player{name: "Vicky"},
               %Player{name: "Paul", wallet: 100},
               %Player{name: "Zen", wallet: 990},
               %Player{name: "Vicky"},
               %Player{name: "Paul", wallet: 100},
               %Player{name: "Zen", wallet: 990}
             ]) == {:error, "Table is constrained to have no more than six players"}
    end
  end

  describe "Table size" do
    test "returns 3 when three spots are filled" do
      assert Table.size(%Table{
               spot_one: %Player{name: "Vicky"},
               spot_two: %Player{name: "Paul", wallet: 100},
               spot_three: %Player{name: "Zen", wallet: 990},
               spot_four: nil,
               spot_five: nil,
               spot_six: nil
             }) == 3
    end

    test "returns 0 when empty table is given" do
      assert Table.size(%Table{}) == 0
    end
  end
end
