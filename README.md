# Pokex

**A poker engine**

Implementing a library to play a [Five Hand Draw](https://en.wikipedia.org/wiki/Draw_poker#Standard_five-card_draw) poker variant.

## Installation

The package can be installed
by adding `pokex` to your list of dependencies in `mix.exs`

* as a hex dependency:

```elixir
def deps do
  [
    {:pokex, "~> 0.2.0"}
  ]
end
```

* or as a git dependency:

```elixir
def deps do
  [
    {:pokex, git: "https://bitbucket.org/vmanoli/pokex", tag: "v0.2.0"}
  ]
end
```

## Play

You can open file `play.exs` for a short script that plays a single simple game.

The script can be played -after cloning the repo- by running:

```console
mix run play.exs
```

or -after getting the dependency- by running:

```console
mix run deps/pokex/play.exs
```

The file `gameplay.exs` also has playing commands using the _Pokex API_.
They are not all implemented yet.

## Documentation

Documentation can be found [here](http://hexdocs.pm/pokex).

_Docs also served with :heart: and [Felix](http://fenixwebserver.com/)_

---

## Roadmap

Steps to be taken later on as time allows.

* Betting and Pot implementation
* Better AI for computer player
* Game server with multiple sessions
* Better testing
* Better docs