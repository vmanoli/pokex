defmodule CardTest do
  use ExUnit.Case
  doctest Card

  describe "Card score:" do
    test "returns correct score for some pip cards" do
      assert Card.score({2, :spades}) == 2
      assert Card.score({5, :diamonds}) == 5
      assert Card.score({10, :hearts}) == 10
    end

    test "returns correct score for all face cards" do
      assert Card.score({:jack, :diamonds}) == 11
      assert Card.score({:queen, :hearts}) == 12
      assert Card.score({:king, :clubs}) == 13
      assert Card.score({:ace, :spades}) == 14
    end
  end

  describe "Highest card first comparison:" do
    test "compares two pip card values" do
      assert Card.highest_first?({10, :spades}, {2, :diamonds}) == true
      assert Card.highest_first?({2, :spades}, {10, :diamonds}) == false
    end

    test "compares two face card values" do
      assert Card.highest_first?({:ace, :spades}, {:jack, :diamonds}) == true
      assert Card.highest_first?({:queen, :hearts}, {:king, :clubs}) == false
    end

    test "compares two card values with one face card and one pip card" do
      assert Card.highest_first?({:ace, :spades}, {2, :diamonds}) == true
      assert Card.highest_first?({8, :hearts}, {:king, :clubs}) == false
    end
  end

  describe "Same value:" do
    test "Returns true if cards are of the same value" do
      assert Card.same_value?({2, :spades}, {2, :diamonds}) == true
      assert Card.same_value?({:ace, :spades}, {:ace, :diamonds}) == true
    end

    test "Returns false if cards are not of the same value" do
      assert Card.same_value?({:ace, :spades}, {2, :diamonds}) == false
      assert Card.same_value?({:ace, :spades}, {:jack, :diamonds}) == false
      assert Card.same_value?({8, :spades}, {2, :diamonds}) == false
    end
  end
end
