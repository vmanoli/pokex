defmodule Dealer do
  @moduledoc """
  Functions to deal and draw cards from the deck

  You can use the `Deck` to `deal()` and `draw()`
  """

  @doc """
  Deal cards to a given number of players.
  Returns a list of card lists for each player hand
  and a list of the rest of the deck.

  The player number should be between 2 and 6.
  """
  def deal(player_number) when player_number in 2..6 do
    Deck.shuffled()
    |> draw(player_number, Application.get_env(:pokex, :card_allotment))
  end

  def deal(player_number) do
    {:error, "Player number should be between 2 and 6. You requested #{player_number}"}
  end

  @doc """
  Draws from a given deck of cards and distributes
  them to players hands, one by one in each turn.
  """
  def draw(deck, player_number, hand_allotment) do
    1..player_number
    |> Enum.map(fn _ -> [] end)
    |> do_draw([], deck, hand_allotment)
  end

  defp do_draw(list_of_waiting_hands, list_of_filled_hands, rest_of_cards, round_num)

  defp do_draw([], list_of_filled_hands, rest_of_cards, 1) do
    %{hands: list_of_filled_hands, rest: rest_of_cards}
  end

  defp do_draw([], [to_be_filled | rest_filled], [next_card | rest_of_cards], round_num) do
    do_draw(
      rest_filled,
      [[next_card | to_be_filled]],
      rest_of_cards,
      round_num - 1
    )
  end

  defp do_draw(
         [waiting | rest_waiting],
         list_of_filled_hands,
         [next_card | rest_of_cards],
         round_num
       ) do
    do_draw(
      rest_waiting,
      [[next_card | waiting] | list_of_filled_hands],
      rest_of_cards,
      round_num
    )
  end
end
