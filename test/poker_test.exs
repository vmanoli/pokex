defmodule PokerTest do
  use ExUnit.Case

  doctest Poker

  describe "Add players to game" do
    test "adds two valid players" do
      assert Poker.players([{"Nick", 1000}, {"Maria", 4000}], %Game{}) ==
               {:ok,
                %Game{
                  players: %Table{
                    spot_one: %Player{name: "Nick", wallet: 1000},
                    spot_two: %Player{name: "Maria", wallet: 4000},
                    spot_three: nil,
                    spot_four: nil,
                    spot_five: nil,
                    spot_six: nil
                  },
                  deck: [],
                  pot: nil
                }}
    end

    test "returns error when adding only one valid player" do
      assert Poker.players([{"Nick", 1000}], %Game{}) ==
               {:error, "Table is constrained to have at least two players"}
    end

    test "returns error when adding more than 6 valid players" do
      assert Poker.players(
               [
                 {"Vicky", 60},
                 {"Paul", 100},
                 {"Zen", 990},
                 {"Vicky", 890},
                 {"Paul", 100},
                 {"Zen", 990},
                 {"Vicky", 990},
                 {"Paul", 100},
                 {"Zen", 990}
               ],
               %Game{}
             ) == {:error, "Table is constrained to have no more than six players"}
    end

    test "adds two valid players when adding 1/3 invalid players in -soft- mode" do
      assert Poker.players([{"Nick", 1000}, {"Maria", 4000}, {"Zen", -990}], %Game{}) ==
               {:ok,
                %Game{
                  players: %Table{
                    spot_one: %Player{name: "Nick", wallet: 1000},
                    spot_two: %Player{name: "Maria", wallet: 4000},
                    spot_three: nil,
                    spot_four: nil,
                    spot_five: nil,
                    spot_six: nil
                  },
                  deck: [],
                  pot: nil
                }}
    end

    test "returns error when adding 1/3 invalid players in -strict- mode" do
      assert Poker.players(
               [{"Nick", 1000}, {"Maria", 4000}, {"Zen", -990}],
               %Game{},
               :add_players_strict
             ) == {:error, "One of the tuples contains invalid player data"}
    end

    test "returns error when adding 2/2 invalid players in -soft- mode" do
      assert Poker.players([{"Nick", -1000}, {"Maria", -4000}], %Game{}) ==
               {:error, "Table is constrained to have at least two players"}
    end

    test "returns error when adding 2/2 invalid players in -strict- mode" do
      assert Poker.players([{"Nick", -1000}, {"Maria", -4000}], %Game{}, :add_players_strict) ==
               {:error, "One of the tuples contains invalid player data"}
    end
  end

  describe "Winner between two players" do
    # setup do
    #   dealer = Dealer.deal(2)
    #   %{dealer: dealer}
    # end

    # test "is returned as a hand", context do
    #   winner = Poker.winner(context.dealer.hands)
    #   assert Enum.count(winner) == 5
    # end
  end

  describe "Exchange one card for a specific spot name" do
    setup do
      game = %Game{
        deck: [
          {6, :clubs},
          {3, :spades},
          {9, :spades},
          {4, :diamonds},
          {6, :hearts},
          {:king, :clubs},
          {3, :diamonds},
          {7, :diamonds},
          {4, :clubs},
          {:king, :diamonds},
          {:jack, :diamonds},
          {:ace, :diamonds},
          {2, :spades},
          {4, :hearts},
          {:jack, :clubs},
          {7, :hearts},
          {:king, :hearts},
          {3, :clubs},
          {:ace, :hearts},
          {8, :spades},
          {:jack, :spades},
          {:queen, :clubs},
          {10, :diamonds},
          {6, :spades},
          {2, :diamonds},
          {6, :diamonds},
          {8, :hearts},
          {5, :clubs},
          {10, :spades},
          {:ace, :spades},
          {5, :spades},
          {:queen, :diamonds},
          {5, :hearts},
          {10, :hearts},
          {:queen, :spades},
          {10, :clubs},
          {4, :spades},
          {3, :hearts},
          {9, :diamonds},
          {:queen, :hearts},
          {7, :spades},
          {8, :diamonds}
        ],
        players: %Table{
          spot_five: nil,
          spot_four: nil,
          spot_one: %Player{
            hand: [
              {5, :diamonds},
              {:jack, :hearts},
              {:king, :spades},
              {8, :clubs},
              {2, :hearts}
            ],
            name: nil,
            wallet: 1000
          },
          spot_six: nil,
          spot_three: nil,
          spot_two: %Player{
            hand: [
              {7, :clubs},
              {2, :clubs},
              {:ace, :clubs},
              {9, :clubs},
              {9, :hearts}
            ],
            name: nil,
            wallet: 4000
          }
        },
        pot: nil
      }

      %{game: game}
    end

    test "returns a game with the hand of the specified Player changed by one card", context do
      {:ok, new_game} = Poker.exchange(context[:game], :spot_one, [{8, :clubs}])

      assert new_game.players.spot_one.hand == [
               {6, :clubs},
               {5, :diamonds},
               {:jack, :hearts},
               {:king, :spades},
               {2, :hearts}
             ]
    end

    test "returns a game with the deck reduced by one card", context do
      {:ok, new_game} = Poker.exchange(context[:game], :spot_one, [{8, :clubs}])

      assert new_game.deck == context[:game].deck -- [{6, :clubs}]
    end
  end
end
