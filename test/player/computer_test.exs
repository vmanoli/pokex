defmodule PlayerComputerTest do
  use ExUnit.Case

  alias Player.Computer

  @tag docs: true, computer: true
  doctest Player.Computer

  describe "Computer player discard generally" do
    test "returns a tuple with two lists" do
      {discard_list, keep_list} =
        Computer.discard([
          {4, :diamonds},
          {:king, :spades},
          {2, :diamonds},
          {:jack, :diamonds},
          {:ace, :diamonds}
        ])

      assert(is_list(discard_list) && is_list(keep_list))
    end

    test "returns exactly five cards in total" do
      {discard_list, keep_list} =
        Computer.discard([
          {4, :diamonds},
          {:king, :spades},
          {2, :diamonds},
          {:jack, :diamonds},
          {:ace, :diamonds}
        ])

      assert(Enum.count(discard_list) + Enum.count(keep_list) == 5)
    end

    test "returns the exact five cards in hand" do
      original_list = [
        {4, :diamonds},
        {:king, :spades},
        {2, :diamonds},
        {:jack, :diamonds},
        {:ace, :diamonds}
      ]

      {discard_list, keep_list} = Computer.discard(original_list)

      assert Enum.concat(discard_list, keep_list) -- original_list == []
    end
  end

  describe "Computer player discard specifically" do
    test "returns the whole hand to keep and empty list to discard for `straight flush`" do
      original_list = [
        {:queen, :diamonds},
        {:king, :diamonds},
        {10, :diamonds},
        {:jack, :diamonds},
        {:ace, :diamonds}
      ]

      {discard_list, keep_list} = Computer.discard(original_list)

      assert discard_list == []
      assert keep_list == original_list
    end

    test "returns the whole hand to keep and empty list to discard for `flush`" do
      original_list = [
        {4, :diamonds},
        {:king, :diamonds},
        {2, :diamonds},
        {:jack, :diamonds},
        {:ace, :diamonds}
      ]

      {discard_list, keep_list} = Computer.discard(original_list)

      assert discard_list == []
      assert keep_list == original_list
    end

    test "returns the longest sequence to keep for `flush`" do
      original_list = [
        {:queen, :diamonds},
        {:king, :diamonds},
        {2, :diamonds},
        {:jack, :diamonds},
        {:ace, :diamonds}
      ]

      {discard_list, _keep_list} = Computer.discard(original_list)

      assert discard_list == [{2, :diamonds}]
    end

    test "returns the whole hand to keep and empty list to discard for `straight`" do
      original_list = [
        {10, :diamonds},
        {:king, :clubs},
        {:queen, :diamonds},
        {:jack, :diamonds},
        {:ace, :hearts}
      ]

      {discard_list, keep_list} = Computer.discard(original_list)

      assert discard_list == []
      assert keep_list == original_list
    end

    test "returns the longest sequence to keep for `straight`" do
      original_list = [
        {10, :diamonds},
        {:king, :diamonds},
        {:queen, :diamonds},
        {:jack, :diamonds},
        {:ace, :hearts}
      ]

      {discard_list, _keep_list} = Computer.discard(original_list)

      assert discard_list == [{:ace, :hearts}]
    end

    test "returns the pairs to keep for `three of a kind`" do
      original_list = [
        {10, :diamonds},
        {5, :diamonds},
        {6, :diamonds},
        {10, :clubs},
        {10, :hearts}
      ]

      {discard_list, keep_list} = Computer.discard(original_list)

      # *to-do create macro assert same lists
      assert discard_list -- [{5, :diamonds}, {6, :diamonds}] == []
      assert keep_list -- [{10, :diamonds}, {10, :clubs}, {10, :hearts}] == []
    end

    test "returns the pairs to keep for `two pairs`" do
      original_list = [
        {:jack, :hearts},
        {3, :spades},
        {:jack, :clubs},
        {3, :clubs},
        {2, :hearts}
      ]

      {discard_list, _keep_list} = Computer.discard(original_list)

      assert discard_list == [{2, :hearts}]
    end

    test "returns the pair to keep for `one pair`" do
      original_list = [
        {7, :hearts},
        {10, :spades},
        {4, :spades},
        {8, :clubs},
        {10, :hearts}
      ]

      {_discard_list, keep_list} = Computer.discard(original_list)

      assert keep_list == [{10, :spades}, {10, :hearts}]
    end

    test "returns the highest sequence for `high card`" do
      original_list = [
        {:queen, :diamonds},
        {3, :hearts},
        {:king, :diamonds},
        {4, :clubs},
        {5, :clubs}
      ]

      {_discard_list, keep_list} = Computer.discard(original_list)

      # *to-do create macro assert same lists
      assert keep_list -- [{3, :hearts}, {4, :clubs}, {5, :clubs}] == []
    end

    test "returns the highest same suit for `high card`" do
      original_list = [
        {:queen, :diamonds},
        {3, :hearts},
        {:king, :diamonds},
        {4, :diamonds},
        {7, :clubs}
      ]

      {_discard_list, keep_list} = Computer.discard(original_list)

      # *to-do create macro assert same lists
      assert keep_list -- [{:queen, :diamonds}, {:king, :diamonds}, {4, :diamonds}] == []
    end
  end
end
