defmodule Card do
  @moduledoc """
  The Card struct and core functions.

  The structure Card contains the fields `value` and `suit`.

  You can get the card's score with `score`
  """

  @type t ::
          {pos_integer, atom}
          | {atom, atom}

  @doc """
  Returns an integer value as the score of each card.

  This depends on the card value i.e. Ace, Jack, 10, 2.

  Integers returned are 2 to 14.
  """
  def score({card_value, _card_suit}) when is_integer(card_value), do: card_value

  def score({card_value, _card_suit}) when is_atom(card_value) do
    case card_value do
      :jack -> 11
      :queen -> 12
      :king -> 13
      :ace -> 14
    end
  end

  @doc """
  Compares two cards and returns true 
  if the first one is of higher value than the second,
  else returns false
  """
  @spec highest_first?(card :: t, card :: t) :: boolean
  def highest_first?({value_1, _suit_1}, {value_2, _suit_2})
      when is_integer(value_1) and is_integer(value_2),
      do: value_1 > value_2

  def highest_first?({value_1, _suit_1}, {value_2, _suit_2})
      when is_atom(value_1) and is_integer(value_2),
      do: true

  def highest_first?({value_1, _suit_1}, {value_2, _suit_2})
      when is_integer(value_1) and is_atom(value_2),
      do: false

  def highest_first?({value_1, _suit_1}, {value_2, _suit_2})
      when is_atom(value_1) and is_atom(value_2) do
    case {value_1, value_2} do
      {:ace, _} -> true
      {:king, :ace} -> false
      {:king, _} -> true
      {:queen, :jack} -> true
      {:queen, _} -> false
      {:jack, _} -> false
    end
  end

  @doc """
  Returns true if the cards are of the same value
  or false if they aren't
  """
  def same_value?({value_1, _suit_1}, {value_2, _suit_2}) do
    value_1 == value_2
  end
end
