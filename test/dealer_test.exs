defmodule DealerTest do
  use ExUnit.Case
  doctest Dealer

  describe "Dealer refuses to deal:" do
    test "to less than two players" do
      assert Dealer.deal(1) ==
               {:error, "Player number should be between 2 and 6. You requested 1"}
    end

    test "to more than six players" do
      assert Dealer.deal(7) ==
               {:error, "Player number should be between 2 and 6. You requested 7"}
    end
  end

  describe "Dealer deals to two players and" do
    setup do
      dealer = Dealer.deal(2)
      %{dealer: dealer}
    end

    test "returns a map with keys :hands and :rest", context do
      assert Map.keys(context.dealer) == [:hands, :rest]
    end

    test "returns 42 remaining cards in :rest", context do
      assert Enum.count(context.dealer.rest) == 42
    end

    test "returns a list of two hands", context do
      assert Enum.count(context.dealer.hands) == 2
    end

    test "returns a list of two hands each containing 5 cards", context do
      [hand_1, hand_2] = context.dealer.hands
      assert Enum.count(hand_1) == 5
      assert Enum.count(hand_2) == 5
    end
  end
end
