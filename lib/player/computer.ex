defmodule Player.Computer do
  @moduledoc """
  The Computer AI player.
  """

  @doc """
  Separates a hand of 5 cards into 2 separate lists, one to keep and one to discard.

  ## Examples

      iex> Player.Computer.discard([
      ...> {:ace, :hearts},
      ...> {2, :clubs},
      ...> {9, :diamonds},
      ...> {:ace, :clubs},
      ...> {4, :hearts} ])
      { [ {2, :clubs}, {9, :diamonds}, {4, :hearts} ], [ {:ace, :clubs}, {:ace, :hearts} ] }

  """
  def discard(hand) do
    with analysis = Hand.analyze(hand),
         rank = Hand.rank_from_analysis(analysis) do
      keep(analysis, rank)
    end
    |> do_discard()
  end

  defp do_discard({from_hand, keepers}) do
    discarded = from_hand -- keepers
    {discarded, keepers}
  end

  # returns tuple of {from_hand, keepers}
  defp keep(hand_analysis, rank)

  # straight flush, # four of a kind, # full house
  defp keep(%{the_hand: hand}, rank) when rank in 1..3 do
    {hand, hand}
  end

  # flush
  defp keep(%{the_hand: hand, sequences: sequence_list}, 4) do
    case Enum.map(sequence_list, &length/1) do
      [1, 4] -> with [_, seq] = sequence_list, do: {hand, seq}
      [4, 1] -> with [seq, _] = sequence_list, do: {hand, seq}
      _ -> {hand, hand}
    end
  end

  # straight
  defp keep(%{the_hand: hand}, 5) do
    suit_lists = Deck.find_same_suits(hand)

    case Enum.map(suit_lists, &length/1) do
      [1, 4] -> with [_, seq] = suit_lists, do: {hand, seq}
      [4, 1] -> with [seq, _] = suit_lists, do: {hand, seq}
      _ -> {hand, hand}
    end
  end

  # three of a kind
  defp keep(%{the_hand: hand, pairs: [pair_list]}, 6) do
    {hand, pair_list}
  end

  # two pairs **to-do if i have four of a suit
  defp keep(%{the_hand: hand, pairs: pair_list}, 7) do
    {hand, Enum.concat(pair_list)}
  end

  # one pair **to-do if i have four of a suit or a four sequence
  defp keep(%{the_hand: hand, pairs: [pair_list]}, 8) do
    {hand, pair_list}
  end

  # high card **to-do check for long sequences and long suits or else keep two highest cards
  defp keep(%{the_hand: hand, sequences: sequence_list}, 9) do
    suit_list = Deck.find_same_suits(hand)

    with highest_suit = Enum.max_by(suit_list, &length/1),
         highest_seq = Enum.max_by(sequence_list, &length/1) do
      if length(highest_suit) > length(highest_seq) do
        {hand, highest_suit}
      else
        {hand, highest_seq}
      end
    end
  end
end
