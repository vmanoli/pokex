
#
# Vanilla version
#

player_names = System.argv() |> Enum.take(2)

player_list =
  if length(player_names) < 2 do
    my_name = IO.gets "What is your name? "
    [
      {String.trim(my_name), 1000}, 
      {"Computer", 4000}
    ]
  else
    [
      {Enum.at(player_names, 0), 1000},
      {Enum.at(player_names, 1), 4000}
    ]
  end

{:ok, game} = Poker.game()

{:ok, game} = Poker.players(player_list, game)

{:ok, game} = Poker.deal(game)

IO.inspect game, label: "Dealed"

# do some simple changes
[first_card, second_card | _] = game.players.spot_one.hand
{:ok, game} = Poker.exchange(game, :spot_one, [first_card, second_card])

# do some AI changes
{discarded, _} = Player.Computer.discard(game.players.spot_two.hand)
{:ok, game} = Poker.exchange(game, :spot_two, discarded)


IO.inspect game, label: "Ended"

{:winner, winning_spot} = Poker.winner(game)

{:ok, winner} = Poker.player(winning_spot, game)

IO.puts "The winner is #{winner.name}!"