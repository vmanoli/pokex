defmodule DeckTest do
  use ExUnit.Case
  doctest Deck

  describe "Deck generates:" do
    test "regular ordered 52-card deck" do
      deck = Deck.regular()
      assert(Enum.count(deck) == 52)
    end

    test "shuffled 52-card deck" do
      deck = Deck.shuffled()
      assert(Enum.count(deck) == 52)
    end
  end

  describe "Card are ordered in:" do
    @card_list [
      {4, :spades},
      {:king, :diamonds},
      {2, :diamonds},
      {:jack, :clubs},
      {:ace, :hearts},
      {:queen, :diamonds},
      {10, :diamonds},
      {5, :clubs}
    ]
    test "Descending ordering (High to Low)" do
      assert Deck.order(@card_list) == [
               {:ace, :hearts},
               {:king, :diamonds},
               {:queen, :diamonds},
               {:jack, :clubs},
               {10, :diamonds},
               {5, :clubs},
               {4, :spades},
               {2, :diamonds}
             ]
    end

    test "Ascending ordering (Low to High)" do
      assert Deck.order(@card_list, :asc) == [
               {2, :diamonds},
               {4, :spades},
               {5, :clubs},
               {10, :diamonds},
               {:jack, :clubs},
               {:queen, :diamonds},
               {:king, :diamonds},
               {:ace, :hearts}
             ]
    end
  end

  describe "Same suit:" do
    test "returns true if a list of cards are of the same suit" do
      assert Deck.is_same_suit([
               {4, :diamonds},
               {:king, :diamonds},
               {2, :diamonds},
               {:jack, :diamonds},
               {:ace, :diamonds}
             ]) == true

      assert Deck.is_same_suit([{4, :diamonds}]) == true
    end

    test "returns false if a list of cards are not of the same suit" do
      assert Deck.is_same_suit([
               {4, :diamonds},
               {:king, :spades},
               {2, :diamonds},
               {:jack, :diamonds},
               {:ace, :diamonds}
             ]) == false

      assert Deck.is_same_suit([
               {4, :diamonds},
               {:king, :diamonds},
               {2, :diamonds},
               {:jack, :diamonds},
               {:ace, :spades}
             ]) == false

      assert Deck.is_same_suit([
               {6, :diamonds},
               {:ace, :spades}
             ]) == false
    end

    test "returns a list of same suit lists" do
      assert Deck.find_same_suits([
               {4, :diamonds},
               {:king, :spades},
               {2, :diamonds},
               {:jack, :diamonds},
               {:ace, :diamonds}
             ]) == [
               [{:king, :spades}],
               [{:ace, :diamonds}, {:jack, :diamonds}, {2, :diamonds}, {4, :diamonds}]
             ]
    end

    test "returns a list with one list of the same card for one card given" do
      assert Deck.find_same_suits([{:ace, :spades}]) == [[{:ace, :spades}]]
    end
  end

  describe "Next card:" do
    test "Returns true if the first card is one step smaller than the second :desc" do
      assert Deck.is_next_card({6, :diamonds}, {7, :spades}, :desc) == true
      assert Deck.is_next_card({:king, :diamonds}, {:ace, :spades}, :desc) == true
      assert Deck.is_next_card({10, :diamonds}, {:jack, :spades}, :desc) == true
      assert Deck.is_next_card({10, :diamonds}, {:jack, :spades}) == true
    end

    test "Returns false if the first card is irrelevant than the second :desc" do
      assert Deck.is_next_card({:king, :diamonds}, {10, :spades}, :desc) == false
      assert Deck.is_next_card({:ace, :diamonds}, {:king, :spades}, :desc) == false
    end

    test "Returns true if the first card is one step bigger than the second :asc" do
      assert Deck.is_next_card({7, :diamonds}, {6, :spades}, :asc) == true
      assert Deck.is_next_card({:ace, :diamonds}, {:king, :spades}, :asc) == true
      assert Deck.is_next_card({:jack, :diamonds}, {10, :spades}, :asc) == true
    end

    test "Returns true if the first card is irrelevant than the second :asc" do
      assert Deck.is_next_card({:king, :diamonds}, {10, :spades}, :asc) == false
      assert Deck.is_next_card({:ace, :diamonds}, {:queen, :spades}, :asc) == false
    end
  end

  describe "Sequences:" do
    test "Always add card on empty sequence list" do
      assert Deck.add_on_sequence_list({10, :diamonds}, []) == [[{10, :diamonds}]]
    end

    test "Add card on single list it does belong to" do
      assert Deck.add_on_sequence_list({10, :diamonds}, [[{9, :spades}]]) ==
               [[{10, :diamonds}, {9, :spades}]]

      assert Deck.add_on_sequence_list({10, :diamonds}, [[{9, :spades}, {8, :clubs}]]) ==
               [[{10, :diamonds}, {9, :spades}, {8, :clubs}]]
    end

    test "Add card as its own list if it does not belong to any provided list" do
      assert Deck.add_on_sequence_list({4, :diamonds}, [[{9, :spades}]]) ==
               [[{4, :diamonds}], [{9, :spades}]]

      assert Deck.add_on_sequence_list({4, :diamonds}, [[{9, :spades}, {8, :clubs}]]) ==
               [[{4, :diamonds}], [{9, :spades}, {8, :clubs}]]
    end

    test "Add card on sequence it does belong to" do
      assert Deck.add_on_sequence_list(
               {10, :diamonds},
               [[{9, :spades}], [{10, :hearts}, {9, :spades}, {8, :clubs}]]
             ) == [[{10, :diamonds}, {9, :spades}], [{10, :hearts}, {9, :spades}, {8, :clubs}]]

      assert Deck.add_on_sequence_list(
               {10, :diamonds},
               [[{10, :hearts}, {9, :spades}, {8, :clubs}], [{9, :spades}, {8, :clubs}]]
             ) == [
               [{10, :hearts}, {9, :spades}, {8, :clubs}],
               [{10, :diamonds}, {9, :spades}, {8, :clubs}]
             ]
    end

    test "Returns empty array in an empty card list" do
      assert Deck.find_sequences([]) == []
    end

    test "Find sequences in a single-card list" do
      assert Deck.find_sequences([{4, :spades}]) == [[{4, :spades}]]
    end

    test "Find sequences in a regular card list" do
      card_list = [
        {4, :spades},
        {:king, :diamonds},
        {2, :diamonds},
        {:jack, :clubs},
        {:ace, :hearts},
        {:queen, :diamonds},
        {10, :diamonds},
        {5, :clubs}
      ]

      assert Deck.find_sequences(card_list) ==
               [
                 [
                   {:ace, :hearts},
                   {:king, :diamonds},
                   {:queen, :diamonds},
                   {:jack, :clubs},
                   {10, :diamonds}
                 ],
                 [
                   {5, :clubs},
                   {4, :spades}
                 ],
                 [
                   {2, :diamonds}
                 ]
               ]
    end

    test "Find sequences in a duplicates card list" do
      card_list = [
        {5, :spades},
        {2, :diamonds},
        {5, :diamonds},
        {5, :hearts},
        {5, :clubs}
      ]

      seqs = Deck.find_sequences(card_list)
      #  [
      #    [{5, :clubs}],
      #    [{5, :hearts}],
      #    [{5, :diamonds}],
      #    [{5, :spades}],
      #    [{2, :diamonds}]
      #  ]
      total_length = Enum.reduce(seqs, 0, &(length(&1) + &2))
      length_array = Enum.map(seqs, &length/1)

      assert length(seqs) == 5
      assert total_length == 5
      assert length_array == [1, 1, 1, 1, 1]
    end
  end

  describe "Pairs:" do
    test "Always add card on empty pair list" do
      assert Deck.add_on_pair_list({10, :diamonds}, []) == [[{10, :diamonds}]]
    end

    test "Add card on single list it does belong to" do
      assert Deck.add_on_pair_list({10, :diamonds}, [[{10, :spades}]]) ==
               [[{10, :diamonds}, {10, :spades}]]

      assert Deck.add_on_pair_list({10, :diamonds}, [[{10, :spades}, {10, :clubs}]]) ==
               [[{10, :diamonds}, {10, :spades}, {10, :clubs}]]
    end

    test "Add card as its own list if it does not belong to any provided list" do
      assert Deck.add_on_pair_list({4, :diamonds}, [[{9, :spades}]]) ==
               [[{4, :diamonds}], [{9, :spades}]]

      assert Deck.add_on_pair_list({4, :diamonds}, [[{8, :spades}, {8, :clubs}]]) ==
               [[{4, :diamonds}], [{8, :spades}, {8, :clubs}]]
    end

    test "Add card on pair it does belong to" do
      assert Deck.add_on_pair_list(
               {10, :diamonds},
               [[{10, :spades}], [{9, :hearts}, {9, :spades}, {9, :clubs}]]
             ) == [
               [{10, :diamonds}, {10, :spades}],
               [{9, :hearts}, {9, :spades}, {9, :clubs}]
             ]

      assert Deck.add_on_pair_list(
               {9, :diamonds},
               [[{10, :hearts}, {10, :spades}, {10, :clubs}], [{9, :spades}, {9, :clubs}]]
             ) == [
               [{10, :hearts}, {10, :spades}, {10, :clubs}],
               [{9, :diamonds}, {9, :spades}, {9, :clubs}]
             ]
    end

    test "Returns empty array in an empty card list" do
      assert Deck.find_pairs([]) == []
    end

    test "Return empty array in a single-card list" do
      assert Deck.find_pairs([{4, :spades}]) == []
    end

    test "Find pairs in a regular card list" do
      card_list = [
        {4, :spades},
        {4, :diamonds},
        {2, :diamonds},
        {:queen, :clubs},
        {4, :hearts},
        {:queen, :diamonds},
        {10, :diamonds},
        {5, :clubs}
      ]

      assert Deck.find_pairs(card_list) ==
               [
                 [
                   {4, :spades},
                   {4, :diamonds},
                   {4, :hearts}
                 ],
                 [
                   {:queen, :clubs},
                   {:queen, :diamonds}
                 ]
               ]
    end

    test "Find pairs in a regular card list using properties" do
      card_list = [
        {5, :spades},
        {2, :diamonds},
        {5, :diamonds},
        {5, :hearts},
        {5, :clubs}
      ]

      seqs = Deck.find_pairs(card_list)
      total_length = Enum.reduce(seqs, 0, &(length(&1) + &2))
      length_array = Enum.map(seqs, &length/1)

      assert length(seqs) == 1
      assert total_length == 4
      assert length_array == [4]
    end
  end
end
