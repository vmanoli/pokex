defmodule Deck do
  @moduledoc """
  Creating a Deck of cards and working with lists of cards.
  """
  @card_values [:ace, :king, :queen, :jack, 10, 9, 8, 7, 6, 5, 4, 3, 2]
  @card_suits [:clubs, :diamonds, :hearts, :spades]

  @reversed_card_values Enum.reverse(@card_values)

  @doc """
  Regular deck creation
  Returns a list of 52 regularly orderd tuples representing the Standard 52-card deck.
  Tuples are of the following format `{value, suit}`.

  ## Examples

      iex> Enum.take( Deck.regular(), 8)
      [                  
        {:ace, :clubs},      
        {:ace, :diamonds},   
        {:ace, :hearts},     
        {:ace, :spades},     
        {:king, :clubs},     
        {:king, :diamonds},  
        {:king, :hearts},    
        {:king, :spades}   
      ]

      iex> Enum.count( Deck.regular())
      52

  """
  def regular() do
    # Enum.reduce(@card_values, [], fn card, deck ->
    #   Enum.reduce(@card_suits, deck, fn suit, deck -> [{card, suit} | deck] end)
    # end)
    for value <- @card_values, suit <- @card_suits do
      {value, suit}
    end
  end

  @doc """
  Returns a shuffled 52-card deck.
  For deck description see @regular
  """
  def shuffled() do
    regular()
    |> Enum.shuffle()
  end

  @doc """
  Orders a list of cards.
  """
  def order(card_list), do: order(card_list, :desc)

  def order(card_list, :desc) do
    Enum.sort(card_list, &Card.highest_first?(&1, &2))
  end

  def order(card_list, :asc) do
    Enum.sort(card_list, &Card.highest_first?(&2, &1))
  end

  @doc """
  Returns true if all cards in card_list are of the same suit
  or false if they aren't
  """
  def is_same_suit(card_list) do
    same_suit(card_list, nil)
  end

  defp same_suit([{_, suit} | rest_of_cards], nil) do
    same_suit(rest_of_cards, suit)
  end

  defp same_suit([], _last_suit), do: true

  defp same_suit([{_, suit} | _rest_of_cards], last_suit) when suit != last_suit, do: false

  defp same_suit([{_, suit} | rest_of_cards], last_suit) when suit == last_suit do
    same_suit(rest_of_cards, last_suit)
  end

  @doc """
  Returns a list of same-suit card lists.

  Cards that are not paired in suit with any other are in one-element lists.
  """
  def find_same_suits(card_list) do
    card_list
    |> Enum.reduce([], &add_on_suit_list/2)
  end

  def add_on_suit_list(card, []), do: [[card]]

  def add_on_suit_list(card, suit_list) do
    result =
      Enum.map_reduce(suit_list, false, fn [last | _rest] = suit, was_added ->
        if is_same_suit([last, card]) do
          {[card | suit], true}
        else
          {suit, was_added}
        end
      end)

    case result do
      {suits, true} -> suits
      {suits, false} -> [[card] | suits]
    end
  end

  @doc """
  Checks if the first card is next* to the value of the second
  * if desc: next is one step smaller
  * if asc: next is one step bigger
  """
  def is_next_card({next_value, _suit_1}, {base_value, _suit_2}, direction \\ :desc) do
    cards =
      case direction do
        :desc ->
          @card_values

        # Enum.reverse(@card_values)
        :asc ->
          @reversed_card_values

        _ ->
          @card_values
      end

    Enum.reduce_while(cards, nil, fn card, acc ->
      if {base_value, next_value} == {acc, card}, do: {:halt, true}, else: {:cont, card}
    end) === true
  end

  @doc """
  Returns a list of sequences of card values found in the `card_list`
  if any. Else returns empty list.

  If given an empty list it returns an empty list.

  *Attention*: this returns Descending order (High to Low)
  """
  def find_sequences(card_list) do
    card_list
    |> order(:asc)
    |> Enum.reduce([], &add_on_sequence_list/2)
  end

  @doc """
  Adds a `card` to the corrsponding sequence in `sequence_list`.

  Returns a list of lists.

  If the card does not belong to any sequence it is added to a new empty list
  at the `sequence_list`.
  """
  def add_on_sequence_list(card, []), do: [[card]]

  def add_on_sequence_list(card, sequence_list) do
    result =
      Enum.map_reduce(sequence_list, false, fn [top | _rest] = sequence, was_added ->
        if is_next_card(top, card, :desc) do
          {[card | sequence], true}
        else
          {sequence, was_added}
        end
      end)

    case result do
      {sequences, true} -> sequences
      {sequences, false} -> [[card] | sequences]
    end
  end

  @doc """
  Returns a list of list of pairs included in `card_list`.
  A *lonely card* (one that does not match with any other) 
  will not be included in the `pair_list` returned
  """
  def find_pairs(card_list) do
    card_list
    |> Deck.order()
    |> Enum.reduce([], &add_on_pair_list/2)
    |> Enum.filter(&(length(&1) > 1))
  end

  @doc """
  Adds a `card` to the corresponding pair list in `pair_list`

  Returns a list of lists.

  If the card does not belong to any pair it is added to a new empty list
  at the `pair_list`.
  """
  def add_on_pair_list(card, []), do: [[card]]

  def add_on_pair_list(card, pair_list) do
    result =
      Enum.map_reduce(pair_list, false, fn [last | _rest] = pair, was_added ->
        if Card.same_value?(last, card) do
          {[card | pair], true}
        else
          {pair, was_added}
        end
      end)

    case result do
      {pairs, true} -> pairs
      {pairs, false} -> [[card] | pairs]
    end
  end
end
