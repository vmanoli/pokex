defmodule Hand do
  @moduledoc """
  Summoning poker specific rules for comparing hands.

  'Deuce-to-seven low' rules apply which means Ace is the highest card.
  """

  @type hand :: [Card.t()]

  @type rank :: 1..9

  @type analysis :: %{
          sequences: [Card.t()],
          pairs: [Card.t()],
          is_same_suit: boolean,
          the_hand: [Card.t()]
        }

  @doc """
  Sorts a list of hands by it's rank.

  Highest ranking cards appear at the head of the list.
  """
  @spec sort_hands(hand_list :: [hand]) :: [hand]
  def sort_hands(hand_list) do
    hand_list
    |> Enum.sort_by(&score/1)
  end

  @doc """
  Returns a high level score based on the rank and relevant cards to the card.

  For example, for Two pairs it takes into consideration the high cards of each of the pairs.
  """
  @spec score(hand :: hand) :: number()
  def score(hand) do
    with hand_analysis <- analyze(hand),
         rank <- rank_from_analysis(hand_analysis) do
      do_score(hand_analysis, rank)
    end
  end

  @doc """
  Returns the rank the hand belongs to.

  Returns an `int` from 1 to 9.

  1 is the highest rank and 9 is the lowest.
  """
  @spec rank(hand :: hand) :: rank
  def rank(hand) do
    hand
    |> analyze()
    |> rank_from_analysis()
  end

  @doc """
  Resolves the rank of a hand from a hand analysis map.

  Returns an `int` from 1 to 9.

  1 is the highest rank and 9 is the lowest.
  """
  @spec rank_from_analysis(hand_analysis :: analysis) :: rank
  def rank_from_analysis(hand_analysis)

  def rank_from_analysis(%{sequences: sequence_list, pairs: pair_list, is_same_suit: is_same_suit}) do
    cond do
      straight_flush?(sequence_list, is_same_suit) -> 1
      four_of_a_kind?(pair_list) -> 2
      full_house?(pair_list) -> 3
      flush?(sequence_list, is_same_suit) -> 4
      straight?(sequence_list, is_same_suit) -> 5
      three_of_kind?(pair_list) -> 6
      two_pair?(pair_list) -> 7
      one_pair?(pair_list) -> 8
      true -> 9
    end
  end

  @doc """
  Analyses the poker hand and returns a map with the sequences, pairs and suit information.

  Sequences is a list, pairs is a list and is_same_suit is a boolean 
  noting whether all cards are of the same suit or not.
  """
  @spec analyze(hand :: hand) :: analysis
  def analyze(hand) do
    sequence_list = Deck.find_sequences(hand)
    pair_list = Deck.find_pairs(hand)
    is_same_suit = Deck.is_same_suit(hand)

    %{sequences: sequence_list, pairs: pair_list, is_same_suit: is_same_suit, the_hand: hand}
  end

  # ==============================================================#
  #
  #              Hand checkers                                   #
  #
  # ==============================================================#

  # Checks if hand is a **Straight flush**
  defp straight_flush?(sequence_list, is_same_suit)
  defp straight_flush?([], _is_same_suit), do: false
  defp straight_flush?(_sequence_list, false), do: false
  defp straight_flush?([sequence], true) when length(sequence) == 5, do: true
  defp straight_flush?(_sequence_list, true), do: false

  # Checks if hand is a **Four of a kind**
  defp four_of_a_kind?(pair_list)
  defp four_of_a_kind?([]), do: false

  defp four_of_a_kind?(pair_list = [pairs | _]) do
    length(pair_list) == 1 && length(pairs) == 4
  end

  # Checks if hand is a *Full house**
  defp full_house?(pair_list) do
    pair_counts = Enum.map(pair_list, &length/1)
    pair_counts == [2, 3] || pair_counts == [3, 2]
  end

  # Checks if hand is a **Flush**
  defp flush?(sequence_list, is_same_suit)
  defp flush?(_sequence_list, false), do: false
  defp flush?([], true), do: true
  defp flush?([sequence], true) when length(sequence) == 5, do: false
  defp flush?(_sequence_list, true), do: true

  # Checks if hand is a **Straigth**
  defp straight?(sequence_list, is_same_suit)
  defp straight?([], _is_same_suit), do: false
  defp straight?(_sequence_list, true), do: false
  defp straight?([sequence], false) when length(sequence) == 5, do: true
  defp straight?(_sequence_list, false), do: false

  # Checks if hand is a **Three of a kind**
  defp three_of_kind?(pair_list)
  defp three_of_kind?([pair]) when length(pair) == 3, do: true
  defp three_of_kind?(_pair_list), do: false

  # Checks if hand is a **Two pair**
  defp two_pair?(pair_list) do
    Enum.map(pair_list, &length/1) == [2, 2]
  end

  # Checks if hand is a **One pair**
  defp one_pair?(pair_list) do
    Enum.map(pair_list, &length/1) == [2]
  end

  # ==============================================================#
  #
  #              Hand Scorers                                     #
  #
  # ==============================================================#

  # I have a concern about steps being floats and not integers...
  @score_steps [60, 4, 0.25, 0.015, 0.001]

  # 1,000-point steps for each rank and n-point steps for each card of importance
  defp do_score(hand_analysis, rank)

  # straight flush
  defp do_score(%{sequences: sequence_list}, 1) do
    [[highest_card | _]] = sequence_list

    1_000 - score_minor_cards(highest_card)
  end

  # four of a kind **to-do: remaining orphan card should be considered in minor score
  defp do_score(%{pairs: pair_list}, 2) do
    [[pair_card | _]] = pair_list

    2_000 - score_minor_cards(pair_card)
  end

  # full house
  defp do_score(%{pairs: pair_list}, 3) do
    [[two_pair_card | _], [three_pair_card | _]] = Enum.sort_by(pair_list, &length/1)

    3_000 - score_minor_cards([three_pair_card, two_pair_card])
  end

  # flush
  defp do_score(%{sequences: sequence_list}, 4) do
    minor_score =
      sequence_list
      |> Enum.concat()
      |> score_minor_cards()

    4_000 - minor_score
  end

  # straight
  defp do_score(%{sequences: sequence_list}, 5) do
    [[highest_card | _]] = sequence_list

    5_000 - score_minor_cards(highest_card)
  end

  # three of a kind **to-do: remaining kicker cards should be considered in minor score
  defp do_score(%{pairs: pair_list}, 6) do
    [[pair_card | _]] = pair_list

    6_000 - score_minor_cards(pair_card)
  end

  # two pairs **to-do: remaining kicker card should be considered in minor score
  defp do_score(%{pairs: pair_list}, 7) do
    [[low_pair_card | _], [high_pair_card | _]] = pair_list

    7_000 - score_minor_cards([high_pair_card, low_pair_card])
  end

  # one pair **to-do: remaining kicker cards should be considered in minor score
  defp do_score(%{pairs: pair_list}, 8) do
    [[pair_card | _]] = pair_list

    8_000 - score_minor_cards(pair_card)
  end

  # high card
  defp do_score(%{sequences: sequence_list}, 9) do
    minor_score =
      sequence_list
      |> Enum.concat()
      |> score_minor_cards()

    9_000 - minor_score
  end

  # card list HAS to be HIGHest -> LOWest importance
  defp score_minor_cards(importance_card_list) when is_list(importance_card_list) do
    importance_card_list
    |> Enum.map(&Card.score/1)
    |> Enum.zip(@score_steps)
    |> Enum.reduce(0, fn {score, weight}, sum -> sum + score * weight end)
  end

  defp score_minor_cards(one_important_card) do
    score_minor_cards([one_important_card])
  end
end
