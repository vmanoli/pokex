defmodule GameTest do
  use ExUnit.Case

  doctest Game

  describe "Add players" do
    test "adds three players in the game" do
      assert Game.add_players(
               %Game{},
               [
                 %Player{name: "Vicky"},
                 %Player{name: "Paul", wallet: 100},
                 %Player{name: "Zen", wallet: 990}
               ]
             ) ==
               {:ok,
                %Game{
                  players: %Table{
                    spot_one: %Player{name: "Vicky"},
                    spot_two: %Player{name: "Paul", wallet: 100},
                    spot_three: %Player{name: "Zen", wallet: 990},
                    spot_four: nil,
                    spot_five: nil,
                    spot_six: nil
                  },
                  deck: [],
                  pot: nil
                }}
    end

    test "returns :error when players are less than 2" do
      assert Game.add_players(%Game{}, [%Player{name: "Zen", wallet: 990}]) ==
               {:error, "Table is constrained to have at least two players"}
    end

    test "returns :error when players are more than 6" do
      assert Game.add_players(
               %Game{},
               [
                 %Player{name: "Vicky"},
                 %Player{name: "Paul", wallet: 100},
                 %Player{name: "Zen", wallet: 990},
                 %Player{name: "Vicky"},
                 %Player{name: "Paul", wallet: 100},
                 %Player{name: "Zen", wallet: 990},
                 %Player{name: "Vicky"},
                 %Player{name: "Paul", wallet: 100},
                 %Player{name: "Zen", wallet: 990}
               ]
             ) == {:error, "Table is constrained to have no more than six players"}
    end
  end

  describe "Deal hands" do
    test "takes a list of hands and distibutes them to the players of the table sequensially" do
    end
  end

  describe "Exchange one card for a specific spot name" do
    setup do
      game = %Game{
        deck: [
          {6, :clubs},
          {3, :spades},
          {9, :spades},
          {4, :diamonds},
          {6, :hearts},
          {:king, :clubs},
          {3, :diamonds},
          {7, :diamonds},
          {4, :clubs},
          {:king, :diamonds},
          {:jack, :diamonds},
          {:ace, :diamonds},
          {2, :spades},
          {4, :hearts},
          {:jack, :clubs},
          {7, :hearts},
          {:king, :hearts},
          {3, :clubs},
          {:ace, :hearts},
          {8, :spades},
          {:jack, :spades},
          {:queen, :clubs},
          {10, :diamonds},
          {6, :spades},
          {2, :diamonds},
          {6, :diamonds},
          {8, :hearts},
          {5, :clubs},
          {10, :spades},
          {:ace, :spades},
          {5, :spades},
          {:queen, :diamonds},
          {5, :hearts},
          {10, :hearts},
          {:queen, :spades},
          {10, :clubs},
          {4, :spades},
          {3, :hearts},
          {9, :diamonds},
          {:queen, :hearts},
          {7, :spades},
          {8, :diamonds}
        ],
        players: %Table{
          spot_five: nil,
          spot_four: nil,
          spot_one: %Player{
            hand: [
              {5, :diamonds},
              {:jack, :hearts},
              {:king, :spades},
              {8, :clubs},
              {2, :hearts}
            ],
            name: nil,
            wallet: 1000
          },
          spot_six: nil,
          spot_three: nil,
          spot_two: %Player{
            hand: [
              {7, :clubs},
              {2, :clubs},
              {:ace, :clubs},
              {9, :clubs},
              {9, :hearts}
            ],
            name: nil,
            wallet: 4000
          }
        },
        pot: nil
      }

      %{game: game}
    end

    test "returns a game with the hand of the specified Player changed by one card", context do
      {:ok, new_game} = Game.exchange(context[:game], :spot_one, [{8, :clubs}])

      assert new_game.players.spot_one.hand == [
               {6, :clubs},
               {5, :diamonds},
               {:jack, :hearts},
               {:king, :spades},
               {2, :hearts}
             ]
    end

    test "returns a game with the deck reduced by one card", context do
      {:ok, new_game} = Game.exchange(context[:game], :spot_one, [{8, :clubs}])

      assert new_game.deck == context[:game].deck -- [{6, :clubs}]
    end

    test "returns the same hand if empty array given", context do
      {:ok, new_game} = Game.exchange(context[:game], :spot_one, [])

      assert new_game.players.spot_one.hand == [
               {5, :diamonds},
               {:jack, :hearts},
               {:king, :spades},
               {8, :clubs},
               {2, :hearts}
             ]
    end

    test "returns five next cards in deck if all cards in hand given", context do
      {:ok, new_game} =
        Game.exchange(context[:game], :spot_one, [
          {5, :diamonds},
          {:jack, :hearts},
          {:king, :spades},
          {8, :clubs},
          {2, :hearts}
        ])

      assert new_game.players.spot_one.hand == [
               {6, :clubs},
               {3, :spades},
               {9, :spades},
               {4, :diamonds},
               {6, :hearts}
             ]
    end

    test "raises exception if more than five cards are given", context do
      assert_raise RuntimeError, fn ->
        Game.exchange(context[:game], :spot_one, [
          {5, :diamonds},
          {:jack, :hearts},
          {3, :spades},
          {:king, :spades},
          {8, :clubs},
          {2, :hearts}
        ])
      end
    end
  end
end
