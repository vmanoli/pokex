defmodule PlayerTest do
  use ExUnit.Case

  doctest Player

  describe "Player wallet" do
    test "raises exception when depositing negative number" do
      assert_raise ArgumentError, fn ->
        Player.deposit(%Player{name: "Nick"}, -10)
      end
    end

    test "raises exception when withdrawing negative number" do
      assert_raise ArgumentError, fn ->
        Player.withdraw(%Player{name: "Nick"}, -10)
      end
    end

    test "raises exception when withdrawing more than wallet amount" do
      assert_raise ArgumentError, fn ->
        Player.withdraw(%Player{name: "Nick", wallet: 100}, -110)
      end
    end
  end
end
