defmodule Game do
  @moduledoc """
  The Game structure and gameplay functions

  Here is where the whole state of a started game is kept. It holds the `players`,
  `deck` and `pot`.

  The `players` attribute is `%Table{}` struct with the game's players. The `deck` is
  a list of cards representing the remaining cards in the hands of the dealer. The `pot` is
  a struct where the bets are kept for each game.

  You can do things such as `Game.add_players/1` klp klp
  """
  defstruct players: %Table{}, deck: [], pot: nil

  @doc """
  Adds the list of players to the `Game.players` in the order they are in the list

  The players are `%Player{}` structs

  Returns `{:ok, game}` with the game struct with the players added or `{:error, "Reason"}`
  """
  def add_players(game, player_list) do
    with {:ok, players} <- Table.init_players(game.players, player_list) do
      {:ok, %Game{game | players: players}}
    else
      {:error, err} -> {:error, err}
    end
  end

  def add_deck(game, deck) do
    %Game{game | deck: deck}
  end

  @doc """
  Takes a list of hands and distributes them to the players of the table sequensially.

  Returns `game` with the game struct with the hands added.
  """
  def deal_hands(game, hand_list) do
    Table.spots()
    |> Enum.zip(hand_list)
    |> Enum.reduce(game, fn {spot, hnd}, game ->
      put_in(
        game,
        [Access.key(:players, %{}), Access.key(spot, %{}), Access.key(:hand, [])],
        hnd
      )
    end)
  end

  @doc """
  Exchanges `card_list` cards in hand for Player in `spot_name` with the top available in deck

  Returns a new game `{:ok, game}` or `{:error, reason}` on failure
  """
  def exchange(game, spot_name, card_list) do
    do_exchange(game, spot_name, card_list)
  end

  defp do_exchange(game, _spot_name, []) do
    {:ok, game}
  end

  defp do_exchange(_game, _spot_name, card_list) when length(card_list) > 5 do
    raise "Card list is not a valid hand"
  end

  defp do_exchange(game, spot_name, card_list) do
    # takes n cards from deck
    {taken, new_deck} = take_from_deck(game, length(card_list))

    # discards cards from hand
    {_prev, game} =
      get_and_update_in(
        game,
        [Access.key(:players, %Table{}), spot_name],
        &{&1, Player.exchange(&1, card_list, taken)}
      )

    # adds cards from deck to hand
    {:ok, %Game{game | deck: new_deck}}
  end

  # Takes the n next cards from deck
  defp take_from_deck(game, n) do
    with taken = Enum.take(game.deck, n),
         new_deck = Enum.drop(game.deck, n) do
      {taken, new_deck}
    end
  end
end
