defmodule Poker do
  @moduledoc """
  A foony module.

  This implements the *Five Card Draw* poker game
  and its respective rules.
  """

  @doc """
  Creates a new game session. It is actually the state of the game.

  ## Examples

      iex> Poker.game()
      {:ok,
      %Game{
        deck: [],
        players: %Table{
          spot_five: nil,
          spot_four: nil,
          spot_one: nil,
          spot_six: nil,
          spot_three: nil,
          spot_two: nil
        },
        pot: nil
      }}
  """
  def game() do
    %Game{}
    |> ok_result()
  end

  @doc """
  Initiates the table of the game with the given player list.

  The player list is a list of tuples in the format `{player_name, money_in_wallet}`.

  If any players where added before will be ignored. This is an initialization.

  Returns {:ok, game} containing the new state of the game ot {:error, reason} on error.
  """
  def players(player_list, game, mode \\ :add_players_soft)

  def players(player_list, game = %Game{}, :add_players_soft) do
    plist =
      player_list
      |> Enum.map(&Player.create/1)
      |> Enum.flat_map(fn pl ->
        case pl do
          {:ok, player} -> [player]
          {:error, _} -> []
        end
      end)

    Game.add_players(game, plist)
  end

  def players(player_list, game = %Game{}, :add_players_strict) do
    plist = Enum.map(player_list, &Player.create/1)

    if Enum.any?(plist, fn player_result -> match?({:error, _}, player_result) end) do
      {:error, "One of the tuples contains invalid player data"}
    else
      Game.add_players(game, plist)
    end
  end

  @doc """
  Deals the cards of the deck to the players of the game.

  Fills the hands of the players.
  Also fills the `Game.deck` property with the rest of the cards.

  Returns {:ok, game} containing the new state of the game ot {:error, reason} on error.
  """
  def deal(game = %Game{}) do
    dealing = game.players |> Table.size() |> Dealer.deal()

    game
    |> Game.add_deck(dealing.rest)
    |> Game.deal_hands(dealing.hands)
    |> ok_result()
  end

  @doc """
  Finds the winning spot amongst the players hands.

  Returns `{:winner, spot}` when there is a definite winner, or `{:tie, [spots]}` with a list of
  spots that tie.
  """
  def winner(game) do
    winner =
      game.players
      |> Table.filled_spots()
      |> Enum.sort_by(fn spot ->
        Hand.score(
          get_in(game, [Access.key(:players, %{}), Access.key(spot, %{}), Access.key(:hand, [])])
        )
      end)
      |> List.first()

    {:winner, winner}
  end

  @doc """
  Returns the Player at the requested spot.

  Returns a `%Player{}` struct.
  """
  def player(spot, game) do
    if spot in Table.spots() do
      {:ok, get_in(game, [Access.key(:players, %{}), Access.key(spot, %{})])}
    else
      {:error, "No such spot defined in table"}
    end
  end

  @doc """
  Exchanges `card_list` cards in hand for Player in `spot_name` with the top available in deck

  Returns a new game `{:ok, game}` or `{:error, reason}` on failure
  """
  def exchange(game = %Game{}, spot_name, card_list) do
    Game.exchange(game, spot_name, card_list)
  end

  # Create properly formatted results
  defp ok_result(result) do
    {:ok, result}
  end
end
